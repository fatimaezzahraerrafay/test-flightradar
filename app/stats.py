from app.packages import *

def active_flights(df, cols):
    """ 
    A function to return active flights in a dataframe
    Considering that an active flight is a flight moving through the air --> "on_ground" == 0
    inputs : 
            - df:  flights dataframe
            - cols: dataframe columns to return
    output : the dataframe of active flights (selected by input columns)
    """
    return df.filter(F.col('on_ground') == 0).select(cols)

def get_max(df, col, by):
    """ 
    A function to return rows with the max value of a column by groups
    inputs : 
            - df: dataframe
            - col: a column of the df that we look for the max value
            - by : column to group the dataframe by
    output : dataframe having the max value
    """
    if by == '':
        max_column = df.agg(F.max(col))\
                       .collect()[0]['max({})'.format(col)]
        df = df.filter(F.col(col) == max_column)
    else:
        w = Window.partitionBy(by)
        df = df.withColumn('max', F.max(col).over(w))\
                                   .filter(F.col(col) == F.col('max'))\
                                   .drop('max')

    return df

def most_active_flights_company(airlines_flights):
    
    airlines_flights_tmp = airlines_flights.groupBy(['airline_iata', 'airline_icao', 'airline_name'])\
                                           .agg(F.count('id').alias('active_flights'))
    airlines_flights_tmp = get_max(airlines_flights_tmp , 'active_flights', '')

    print('Q1- The company with the most active flights in the world is: ')
    airlines_flights_tmp.show()
    return None


def most_regional_active_flights_companies_by_continent(flights_df, airports_df, airlines_active_flights_df):
    
    active_flights_df = active_flights(flights_df, ["id", "origin_airport_iata", "destination_airport_iata"])
    airports_origin = airports_df.select("iata", "continent")\
                                 .toDF(*["origin_airport_iata", "origin_continent"])
    airports_destination = airports_df.select("iata", "continent")\
                                      .toDF(*["destination_airport_iata", "destination_continent"])
    active_flights_df = active_flights_df.join(airports_origin, "origin_airport_iata", how='left')\
                                         .join(airports_destination, "destination_airport_iata", how='left')\
                                         .na.drop(subset=['origin_continent'])\
                                         .filter(F.col('origin_continent') != 'NaN')\
                                         .drop_duplicates()
    active_flights_df = active_flights_df.select("id", "origin_airport_iata", "origin_continent", "destination_airport_iata", "destination_continent")
    
    regional_active_flights_df = active_flights_df.filter(active_flights_df['origin_continent'] == active_flights_df['destination_continent'])

    regional_active_flights_df = active_flights_df.join(airlines_active_flights_df, "id", how = "left")

    count_regional_active_flights_df = regional_active_flights_df.groupBy(['origin_continent', 'airline_iata', 'airline_icao', 'airline_name'])\
                                                                 .agg(F.count('id').alias('active_flights'))

    most_active_airline_by_continent = get_max(count_regional_active_flights_df, 'active_flights','origin_continent' )
    most_active_airline_by_continent = most_active_airline_by_continent.select('origin_continent', 'airline_iata', 'airline_icao', 'airline_name', 'active_flights')
    most_active_airline_by_continent = most_active_airline_by_continent.toDF(*['continent', 'airline_iata', 'airline_icao', 'airline_name', 'active_flights'])
    most_active_airline_by_continent = most_active_airline_by_continent.sort(F.col('active_flights').desc())

    print('Q2: By continent, companies with the most regional active flights (airports of Origin & Destination within the same continent) are : ')
    most_active_airline_by_continent.show()
    return None


def active_flight_with_longest_route(flights_df):
    active_flights_df = active_flights(flights_df, flights_df.toPandas().columns.tolist())\
                        .filter(F.col('route_lenght') != 'NaN')
    active_flight_longest_route = get_max(active_flights_df, 'route_lenght', '')

    print('Q3: Active flight that has the longest route is :')
    active_flight_longest_route.show()

    return None

def average_route_distance_by_continent(flights_df, airports_df):
    flights_route_df = flights_df.select("id", "origin_airport_iata", "route_lenght")\
                                 .filter(F.col('route_lenght') != 'NaN')

    airports_origin = airports_df.select("iata", "continent")\
                                 .toDF(*["origin_airport_iata", "origin_continent"])    
    root_distance_by_continent = flights_route_df.join(airports_origin, "origin_airport_iata", how='inner')\
                                                 .na.drop(subset=['origin_continent'])\
                                                 .filter(F.col('origin_continent') != 'NaN')

    avg_root_distance_by_continent = root_distance_by_continent.groupby(["origin_continent"])\
                                                               .agg(F.avg("route_lenght"))\
                                                               .toDF(*["continent", "average_route_distance"])

    print('Q4: By continent, the average route distance (flight localization by airport of origin) is :')
    avg_root_distance_by_continent.show()
    return None
    

def airplane_manufacturer_with_most_active_flights(flights_df):
    manufacterer_active_flights = active_flights(flights_df, ['manufacturer', 'id']).filter(F.col('manufacturer') != 'NaN')
    manufacterer_active_flights = manufacterer_active_flights.groupBy('manufacturer')\
                                                             .agg(F.count("id").alias('active_flights'))    
    manufacterer_active_flights = get_max(manufacterer_active_flights, 'active_flights', '')

    print('Q5.1: The leading airplane manufacturer having the most active flights in the world is :')
    manufacterer_active_flights.show()
    return None


def most_frequent_airplane_model_by_continent(flights_df, airports_df):
    
    flights_model_df = flights_df.select("id", "origin_airport_iata", "model")\
                                 .na.drop(subset=['model'])\
                                 .filter(F.col('model') != 'NaN')
    airports_origin = airports_df.select("iata", "continent")\
                                 .toDF(*["origin_airport_iata", "origin_continent"])\
                                 .filter(F.col('continent') != 'NaN')    
    model_by_continent = flights_model_df.join(airports_origin, "origin_airport_iata", how='inner')
    model_by_continent = model_by_continent.groupby(["origin_continent", "model"]).agg(F.count("id").alias('active_flights'))    

    freq_model_by_continent = get_max(model_by_continent, 'active_flights', 'origin_continent')
    freq_model_by_continent = freq_model_by_continent.toDF(*['continent', 'airplane_model', 'active_flights'])
    freq_model_by_continent = freq_model_by_continent.sort(F.col('active_flights').desc())

    print('Q5.2: By continent, the most frequent airplane model (airplane localization by airport of origin) is : ')
    freq_model_by_continent.show()
    return None


def top_three_airplanes_model_by_company_registration_country(flights_df):
    registration_flights = active_flights(flights_df, ['registration', 'model', 'id'])
    registration_flights = registration_flights.filter(F.col('model') != 'NaN')\
                                               .groupby(['registration', 'model'])\
                                               .agg(F.count('id').alias('active_flights'))
                         
    partition = Window.partitionBy(F.col('registration'))\
                      .orderBy(F.col('active_flights').desc())

    registration_flights = registration_flights.withColumn("row",F.row_number().over(partition))\
                                               .filter(F.col("row") <= 3).drop("row")
    print('Q6: By company registration country, the tops 3 airplanes model flying are :')
    registration_flights.show()
    return None


def most_popular_destination_airport_by_continent(flights_df, airports_df):
    flights_destination = flights_df.select(["id", "destination_airport_iata"])
    airports_destination = airports_df.select("iata", "name", "continent")\
                                      .toDF(*["destination_airport_iata", "airport_name", "continent"])

    flights_destination = flights_destination.join(airports_destination, "destination_airport_iata", how='left')\
                                             .filter((F.col('continent') != 'NaN') & (F.col('continent').isNotNull()))\
                                             .filter((F.col('airport_name') != 'NaN') & (F.col('airport_name').isNotNull()))\
                                             .filter(F.col('destination_airport_iata') != 'NaN')                     

    flights_destination = flights_destination.groupby(["continent", "airport_name", "destination_airport_iata"])\
                                             .agg(F.count("id").alias('active_flights'))    

    freq_airport_by_continent = get_max(flights_destination, 'active_flights', 'continent')
    freq_airport_by_continent = freq_airport_by_continent.toDF(*['continent', 'airport_name', 'airport_iata', 'active_flights'])
    freq_airport_by_continent = freq_airport_by_continent.sort(F.col('active_flights').desc())

    print('Q7.1: By continent, the airport which is the most popular destination is :')
    freq_airport_by_continent.show()
    return None


def airport_with_greatest_flights_difference(flights_df, airports_df):
    airports_df = airports_df.select("iata", "name").toDF(*["iata", "airport_name"])
    inbound_flights = flights_df.select(["id", "origin_airport_iata"]).toDF(*['id', 'iata'])
    outbound_flights = flights_df.select(["id", "destination_airport_iata"]).toDF(*['id', 'iata'])

    inbound_flights = inbound_flights.join(airports_df, 'iata', how='left')\
                                     .groupBy(['iata', 'airport_name'])\
                                     .agg(F.count("id").alias('active_flights_in')) 
    outbound_flights = outbound_flights.join(airports_df, 'iata', how='left')\
                                     .groupBy(['iata', 'airport_name'])\
                                     .agg(F.count("id").alias('active_flights_out'))

    greatest_airport_df = inbound_flights.join(outbound_flights, ['iata', 'airport_name'], how='inner')\
                                         .withColumn('active_flights', F.abs(F.col('active_flights_in') - F.col('active_flights_out')))
    
    greatest_airport_df = get_max(greatest_airport_df, 'active_flights', '')

    print('Q7.2: The airport with the greatest inbound/outbound flights difference (positive or negative) is :')
    greatest_airport_df.show()
    return None


def average_active_flight_speed_by_continent():
    return {}