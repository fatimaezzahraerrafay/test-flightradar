FROM godatadriven/pyspark:latest
COPY requirements.txt .

RUN pip3 install -r requirements.txt
ENTRYPOINT ["python", "main.py"]