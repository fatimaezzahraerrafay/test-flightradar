from app.packages import *

#Initiate spark
spark = SparkSession.builder.appName("test").getOrCreate()

#Instance API
fr_api = FlightRadar24API() 


# https://www.geeksforgeeks.org/python-calculate-distance-between-two-places-using-geopy/
def geodesic_distance(x):
    """A function to calculate the length of the path between 2 points (the geodesic distance from latitude-longitude data)
    x : a pandas serie contains latitude and longitude coordonates of the 2 points"""
    x['route_lenght'] = geodesic(
        (x['origin_airport_lat'], x['origin_airport_lon']),
        (x['destination_airport_lat'], x['destination_airport_lon'])
    ).km
    return x


def extract():
    """A function to extract data from the API""" 
    flights = fr_api.get_flights() 
    airlines = fr_api.get_airlines()
    airports = fr_api.get_airports()
    zones = fr_api.get_zones()

    return {
        "flights": flights,
        "airlines": airlines,
        "airports": airports,
        "zones": zones
    }


def transform(extracted_data):    
    """A function to transform the extracted data
    The result is a dictionary of dataframes """ 

    # transform the extracted data to spark dataframes    
    flights_df = spark.createDataFrame(extracted_data["flights"])
    airlines_df = spark.createDataFrame(extracted_data["airlines"])

    # add a new dataframe corresponding to the associations between airlines and active flights 
    # some values of iata and icao are null, so there is no unique key to search the airline
    # I then separate the dataframe and search for the airline according to the icao and then according to the iata,
    # and then join the two dataframes
    flights_iata = flights_df.filter(flights_df['on_ground'] == 0).select('airline_iata', 'id') 
    flights_icao = flights_df.filter(flights_df['on_ground'] == 0).select('airline_icao', 'id')
    airlines_tmp = airlines_df.toDF(*['airline_iata', 'airline_icao', 'airline_name']) # rename airlines colums
    airlines_tmp_iata = airlines_tmp.join(flights_iata, 'airline_iata', how='left') # flights by iata 
    airlines_tmp_icao = airlines_tmp.join(flights_icao, 'airline_icao', how='left') # flights by icao
    airlines_tmp_icao = airlines_tmp_icao.select('airline_iata', 'airline_icao', 'airline_name', 'id')
    airlines_active_flights_df = airlines_tmp_iata.union(airlines_tmp_icao).drop_duplicates(subset=['id'])

    # the airports dictionnary extracted contains a columns (alt, lat, loan) with different values types  
    # Then, I defined a structure shema to cast the type of the concerned columns   
    airports_schema = StructType([StructField("alt", StringType(), True),
                              StructField("country", StringType(), True),
                              StructField("iata", StringType(), True),
                              StructField("icao", StringType(), True),
                              StructField("lat", StringType(), True),
                              StructField("lon", StringType(), True),
                              StructField("name", StringType(), True)])
    airports_df = spark.createDataFrame(extracted_data["airports"], airports_schema)
    airports_df = airports_df.withColumn("lat",airports_df.lat.cast('double'))
    airports_df = airports_df.withColumn("lon",airports_df.lon.cast('double'))

    # add new column of each flight's route length (distance between origin and destination airport)
    flights_tmp = flights_df.select('id', 'origin_airport_iata', 'destination_airport_iata')
    airports_coord = airports_df.select('iata', 'lat', 'lon')
    origin_coord = airports_coord.toDF(*['origin_airport_iata', 'origin_airport_lat', 'origin_airport_lon'])
    destination_coord = airports_coord.toDF(*['destination_airport_iata', 'destination_airport_lat', 'destination_airport_lon'])
    flights_tmp = flights_tmp.join(origin_coord, 'origin_airport_iata', how = 'inner')\
                             .join(destination_coord, 'destination_airport_iata', how = 'inner')
    flights_tmp = flights_tmp.toPandas().apply(lambda x: geodesic_distance(x), axis = 1)
    flights_tmp = spark.createDataFrame(flights_tmp).select('id', 'route_lenght')
    flights_df = flights_df.join(flights_tmp, 'id', how = 'left')
    
    # add column model to the flights dataframe (the model of the aircrafts corresponding to the flight)
    models = []
    for id in flights_df.select('id').collect():
        details = fr_api.get_flight_details(id['id'])
        try:
            models.append((id['id'], details['aircraft']['model']['text']))
        except:
            pass

    models_df = spark.createDataFrame(models, ['id', 'model'])
    flights_df = flights_df.join(models_df, 'id', how= 'left')

    # add a column of manufacturer to the flights dataframe 
    flights_df = flights_df.withColumn("manufacturer", F.split(flights_df.model, " ").getItem(0))

    # From the zones dictionnary, I extracted continents with corresponding boundaries
    continents_bounds = {
        continent : {
            'br_x': float(extracted_data["zones"][continent]['br_x']),
            'br_y': float(extracted_data["zones"][continent]['br_y']),
            'tl_x' : float(extracted_data["zones"][continent]['tl_x']),
            'tl_y' : float(extracted_data["zones"][continent]['tl_y'])
        }
        for continent in extracted_data["zones"].keys()
    }

    # Add a new column continent to the airports dataframe
    airports_df = airports_df.withColumn("continent", F.lit(None).cast(StringType())) # Initialize the column with None value
    for continent, bound in continents_bounds.items():
        airports_df = airports_df.withColumn(
            "continent", F.when(
                (airports_df.lon >= float(bound['tl_x'])) &
                (airports_df.lon <= float(bound['br_x'])) &
                (airports_df.lat >= float(bound['br_y'])) &
                (airports_df.lat <= float(bound['tl_y'])),
                continent
            ).otherwise(airports_df.continent)
        )

    return {
        "flights": flights_df,
        "airlines": airlines_df,
        "airlines_flights": airlines_active_flights_df,
        "airports": airports_df
    }



def load(transformed_data):
    """A function to load the transformed data into a csv files"""
    path = "data/" 
    files = os.listdir(path)
    print(files)
    for file, file_df in transformed_data.items():
        file_name = '{}.csv'.format(file)
        if file_name in files:
            os.remove(path + file_name)
        else:
            pass
        file_df.toPandas().to_csv(path + file_name, sep = ';', index = False)

    return None


def etl():
    """A function of the ETL pipeline (Extracting data from the API, transform it and load it into csv files)"""
    extract_data = extract()
    transform_data = transform(extract_data)
    extract_data = extract()
    load(transform_data)
    
    return None
