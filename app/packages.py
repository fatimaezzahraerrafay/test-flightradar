from FlightRadar24.api import FlightRadar24API
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, DoubleType, IntegerType, StringType
from pyspark.sql import functions as F
from pyspark.sql.window import Window
import pandas as pd
from geopy.distance import geodesic
import os