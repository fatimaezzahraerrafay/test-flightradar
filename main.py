from app.etl import *
from app.stats import *

def main():
    
    # ETL
    etl()


    # Read loaded files
    flights_df = pd.read_csv('data/flights.csv', delimiter = ';', usecols = ['id', 'airline_iata', 'airline_icao', 'destination_airport_iata', 'on_ground', 'origin_airport_iata',	'registration', 'route_lenght', 'model', 'manufacturer'])
    airlines_df = pd.read_csv('data/airlines.csv', delimiter = ';', usecols = ['Code', 'ICAO', 'Name'])
    airports_df = pd.read_csv('data/airports.csv', delimiter = ';', usecols = ['iata', 'icao', 'name', 'continent'])
    airlines_flights_df = pd.read_csv('data/airlines_flights.csv', delimiter = ';', usecols = ['airline_iata', 'airline_icao', 'airline_name', 'id'])

    flights_schema = StructType([StructField("id", StringType(), True),
                            StructField("airline_iata", StringType(), True),
                            StructField("airline_icao", StringType(), True),
                            StructField("destination_airport_iata", StringType(), True),
                            StructField("on_ground", IntegerType(), True),
                            StructField("origin_airport_iata", StringType(), True),
                            StructField("registration", StringType(), True),
                            StructField("route_lenght", DoubleType(), True),
                            StructField("model", StringType(), True),
                            StructField("manufacturer", StringType(), True)])
    flights_df = spark.createDataFrame(flights_df, flights_schema)

    airlines_schema = StructType([StructField("Code", StringType(), True),
                            StructField("ICAO", StringType(), True),
                            StructField("Name", StringType(), True)])
    airlines_df = spark.createDataFrame(airlines_df, airlines_schema)

    airports_schema = StructType([StructField("iata", StringType(), True),
                            StructField("icao", StringType(), True),
                            StructField("name", StringType(), True),
                            StructField("continent", StringType(), True)])
    airports_df = spark.createDataFrame(airports_df, airports_schema)

    airlines_flights_schema = StructType([StructField("airline_iata", StringType(), True),
                            StructField("airline_icao", StringType(), True),
                            StructField("airline_name", StringType(), True),
                            StructField("id", StringType(), True)])
    airlines_flights_df = spark.createDataFrame(airlines_flights_df, airlines_flights_schema)
        

    # Answers
    most_active_flights_company(airlines_flights_df)

    most_regional_active_flights_companies_by_continent(flights_df, airports_df, airlines_flights_df)
    
    active_flight_with_longest_route(flights_df)
    
    average_route_distance_by_continent(flights_df, airports_df)
    
    airplane_manufacturer_with_most_active_flights(flights_df)
    
    most_frequent_airplane_model_by_continent(flights_df, airports_df)

    top_three_airplanes_model_by_company_registration_country(flights_df)
    
    most_popular_destination_airport_by_continent(flights_df, airports_df)
    
    airport_with_greatest_flights_difference(flights_df, airports_df)
    
    average_active_flight_speed_by_continent()


if __name__ == "__main__":
    main()
    